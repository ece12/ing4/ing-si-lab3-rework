const db = require('./db_config');
const {
	listAllChannels,
	createChannel,
	getChannel,
	updateChannel,
	deleteChannel,
} = require('./models/channel');
const {} = require('./models/user');
const {} = require('./models/message');
//https://codeburst.io/es6-destructuring-the-complete-guide-7f842d08b98f

//Cette fonction permet de reset la base de données entre chaque test. Dans l'idéal, il faudra une base de données spécfifique pour les tests...
const clearDB = async () => {
	await db.clear();
};

module.exports = {
	channels: {
		create: createChannel, //Il ne faut pas faire create: createChannel() !! On ne veut donner que la référence de la fonction, pas l'executer !
		list: listAllChannels,
		get: getChannel,
		update: updateChannel,
		delete: deleteChannel,
	},
	users: {
		//add user methods
	},
	messages: {
		//add message methods
	},
	admin: {
		clear: clearDB,
	},
};
