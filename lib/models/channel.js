const {v4: uuid} = require('uuid');
const {merge} = require('mixme');

const db = require('../db_config');

const listAllChannels = async () => {
	return new Promise((resolve, reject) => {
		const channels = [];

		const options = {
			gt: "channels:",
			lte: "channels" + String.fromCharCode(":".charCodeAt(0) + 1),
		};

		//https://github.com/Level/level#createReadStream
		db.createReadStream(options)
			.on('data', ({key, value}) => {
				let channel = JSON.parse(value);
				channel.id = key;
				channels.push(channel);
			})
			.on('error', (err) => {
				reject(err)
			})
			.on('end', () => {
				resolve(channels);
			});
	})
};

const createChannel = async channel => {
	if(!channel.name) {
		throw Error('Invalid channel');
	}

	const id = uuid();

	//https://github.com/Level/level#put
	await db.put(`channels:${id}`, JSON.stringify(channel));

	return merge(channel, {id: id});
};

const getChannel = async channelId => {
	//ne pas oublier le blindage dans le cas où le channel id serait "faux" => la clé n'existe pas dans le database

	//https://github.com/Level/level#get
};

const updateChannel = async (channelId, channel) => {
	//ne pas oublier le blindage dans le cas où le channel id serait "faux" => la clé n'existe pas dans le database

	//Réflechir niveau algo. on ne peut que get et put des données mais pas directement les modifier !
};

const deleteChannel = async channelId => {
	//ne pas oublier le blindage dans le cas où le channel id serait "faux" => la clé n'existe pas dans le database

	//https://github.com/Level/level#del
};

module.exports = {
	createChannel, //Il ne faut pas faire create: createChannel() !! On ne veut donner que la référence de la fonction, pas l'executer !
	listAllChannels,
	getChannel,
	updateChannel,
	deleteChannel,
};
