const express = require('express');
const db = require('./db');
const app = express();

app.use(require('body-parser').json()); //Permet de lire du json. Ne pas oublier d'avoir le header Content-Type avec comme valeur application/json
//https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Type

app.get('/', (req, res) => {
	res.send([
		'<h1>ECE DevOps Chat</h1>'
	].join(''))
});

/**
 * Channels API
 */

app.get('/channels', async (req, res) => {
	const channels = await db.channels.list();
	res.json(channels);
});

app.post('/channels', async (req, res) => {
	const channel = await db.channels.create(req.body);
	res.status(201).json(channel);
});

app.get('/channels/:id', (req, res) => {
	const channel = db.channels.get(req.body);
	res.json(channel);
});

app.put('/channels/:id', (req, res) => {
	const channel = db.channels.update(req.body);
	res.json(channel);
});

//delete channel

/**
 * Users API
 */

//get all users
//create user (register)
//get user
//update user
//delete user (only me can delete the account)

/**
 * Messages API
 */

//get all messages from channel
//create message from channel
//update message from channel
//delete message from channel

module.exports = app;
